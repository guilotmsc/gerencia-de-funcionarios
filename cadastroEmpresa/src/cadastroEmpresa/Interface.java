package cadastroEmpresa;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Interface {

	private JFrame frame;
	final Funcionario funcionario = new Funcionario();

	private JTextField arquivoImportar;
	private JTextField arquivoExportar;
	private JTable table;
	private JTextField textField;

	/**
	 * Inicializa��o da aplica��o.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try { 
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Cria��o da aplica��o.
	 */
	public Interface() {
		inicializar();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void inicializar() {

		frame = new JFrame();
		frame.setBounds(100, 100, 546, 618);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		table = new JTable();
		table.setFillsViewportHeight(true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();

				funcionario.getNomeFuncionario().setText(model.getValueAt(table.getSelectedRow(), 0).toString());
				funcionario.getNrRegistro().setText((String) model.getValueAt(table.getSelectedRow(), 1));
				funcionario.getNrCpf().setText((String) model.getValueAt(table.getSelectedRow(), 2));
				funcionario.getNrRg().setText((String) model.getValueAt(table.getSelectedRow(), 3));
				funcionario.getCargo().setText((String) model.getValueAt(table.getSelectedRow(), 4));
				funcionario.getVlSalario().setText((String) model.getValueAt(table.getSelectedRow(), 5));
			}
		});
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Nome", "CPF", "RG", "Registro", "Cargo", "Sal�rio" }));
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setBounds(30, 264, 463, 236);
		frame.getContentPane().add(table);

		JLabel lblNewLabel = new JLabel("Nome:");
		lblNewLabel.setBounds(36, 43, 46, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(36, 105, 46, 14);
		frame.getContentPane().add(lblCpf);

		JLabel lblRg = new JLabel("RG:");
		lblRg.setBounds(36, 135, 46, 14);
		frame.getContentPane().add(lblRg);

		JLabel lblRegistro = new JLabel("Registro:");
		lblRegistro.setBounds(296, 105, 46, 14);
		frame.getContentPane().add(lblRegistro);

		JLabel lblCargo = new JLabel("Cargo:");
		lblCargo.setBounds(36, 75, 46, 14);
		frame.getContentPane().add(lblCargo);

		JLabel lblSalario = new JLabel("Sal\u00E1rio:");
		lblSalario.setBounds(296, 75, 46, 14);
		frame.getContentPane().add(lblSalario);

		JLabel lblArquivoExportar = new JLabel("Arquivo:");
		lblArquivoExportar.setBounds(64, 515, 46, 14);
		frame.getContentPane().add(lblArquivoExportar);

		JLabel lblArquivoImportar = new JLabel("Arquivo:");
		lblArquivoImportar.setBounds(64, 549, 46, 14);
		frame.getContentPane().add(lblArquivoImportar);

		funcionario.getNomeFuncionario().setBounds(92, 40, 347, 20);
		frame.getContentPane().add(funcionario.getNomeFuncionario());
		funcionario.getNomeFuncionario().setColumns(10);

		funcionario.getNrRegistro().setBounds(350, 100, 89, 20);
		funcionario.getNrRegistro().setColumns(10);
		frame.getContentPane().add(funcionario.getNrRegistro());

		funcionario.getVlSalario().setBounds(350, 71, 89, 20);
		funcionario.getVlSalario().setColumns(10);
		frame.getContentPane().add(funcionario.getVlSalario());

		funcionario.getNrCpf().setBounds(92, 100, 195, 20);
		funcionario.getNrCpf().setColumns(10);
		frame.getContentPane().add(funcionario.getNrCpf());

		funcionario.getNrRg().setBounds(92, 130, 195, 20);
		funcionario.getNrRg().setColumns(10);
		frame.getContentPane().add(funcionario.getNrRg());

		funcionario.getCargo().setBounds(92, 72, 195, 20);
		funcionario.getCargo().setColumns(10);
		frame.getContentPane().add(funcionario.getCargo());
		
		arquivoImportar = new JTextField();
		arquivoImportar.setColumns(10);
		arquivoImportar.setBounds(120, 546, 182, 20);
		frame.getContentPane().add(arquivoImportar);

		arquivoExportar = new JTextField();
		arquivoExportar.setColumns(10);
		arquivoExportar.setBounds(120, 512, 182, 20);
		frame.getContentPane().add(arquivoExportar);

		textField = new JTextField();
		textField.setBounds(30, 233, 166, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JButton btnSalvar = new JButton("SALVAR");
		btnSalvar.setBounds(193, 170, 89, 23);
		frame.getContentPane().add(btnSalvar);

		JButton btnEditar = new JButton("EDITAR");
		btnEditar.setBounds(292, 170, 89, 23);
		frame.getContentPane().add(btnEditar);

		JButton btnRemover = new JButton("REMOVER");
		btnRemover.setBounds(391, 170, 102, 23);
		frame.getContentPane().add(btnRemover);

		/* Adicionando elemento. */
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { funcionario.getNomeFuncionario().getText(),
						funcionario.getNrRegistro().getText(), funcionario.getNrCpf().getText(),
						funcionario.getNrRg().getText(), funcionario.getCargo().getText(),
						funcionario.getVlSalario().getText() });

				limpar();
			}
		});

		/* Alterando elemento. */
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();

				if (table.getSelectedRow() == -1) {
					System.out.println("Tabela vazia");
				} else {
					model.setValueAt(funcionario.getNomeFuncionario().getText(), table.getSelectedRow(), 0);
					model.setValueAt(funcionario.getNrRegistro().getText(), table.getSelectedRow(), 1);
					model.setValueAt(funcionario.getNrCpf().getText(), table.getSelectedRow(), 2);
					model.setValueAt(funcionario.getNrRg().getText(), table.getSelectedRow(), 3);
					model.setValueAt(funcionario.getCargo().getText(), table.getSelectedRow(), 4);
					model.setValueAt(funcionario.getVlSalario().getText(), table.getSelectedRow(), 5);

					limpar();
				}
			}
		});

		/* Removendo elemento. */
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();

				model.removeRow(table.getSelectedRow());

				limpar();
			}
		});

		frame.setVisible(true);

		/**
		 * Invoca o m�todo que faz a exporta��o para arquivo de texto.
		 */
		JButton btnExportar = new JButton("EXPORTAR");
		btnExportar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { 
				exportarTxt();
			}
		});
		btnExportar.setBounds(318, 511, 108, 23);
		frame.getContentPane().add(btnExportar);

		/**
		 * Invoca o m�todo que faz a importa��o de arquivo de texto.
		 */
		JButton btnImportar = new JButton("IMPORTAR");
		btnImportar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importarTxt();
			}
		});
		btnImportar.setBounds(318, 545, 108, 23);
		frame.getContentPane().add(btnImportar);

		
		/* Efetua pesquisa na grid */
		JButton btnPesquisar = new JButton("PESQUISAR");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TableRowSorter<TableModel> sorter = null;
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				sorter = new TableRowSorter<TableModel>(model);
				table.setRowSorter(sorter);

				String text = textField.getText();
				if (text.length() == 0) {
					sorter.setRowFilter(null);
				} else {
					sorter.setRowFilter(RowFilter.regexFilter(text));
				}
			}
		});
		btnPesquisar.setBounds(206, 232, 112, 23);
		frame.getContentPane().add(btnPesquisar);

	}

	public void limpar() {
		funcionario.getNomeFuncionario().setText("");
		funcionario.getNrRegistro().setText("");
		funcionario.getVlSalario().setText("");
		funcionario.getNrCpf().setText("");
		funcionario.getNrRg().setText("");
		funcionario.getCargo().setText("");
	}

	public void exportarTxt() {
		String filename = arquivoExportar.getText() + ".txt";

		BufferedWriter bfw;
		try {
			bfw = new BufferedWriter(new FileWriter(filename));

			for (int i = 0; i < table.getColumnCount(); i++) {
				bfw.write(table.getColumnName(i));
				bfw.write("\t");
			}

			for (int i = 0; i < table.getRowCount(); i++) {
				bfw.newLine();
				for (int j = 0; j < table.getColumnCount(); j++) {
					bfw.write((String) (table.getValueAt(i, j)));
					bfw.write("\t");
					;
				}
			}
			bfw.close();
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	public void importarTxt(){
		try {
			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new FileReader(arquivoImportar.getText() + ".txt"));
			
			DefaultTableModel modelo = (DefaultTableModel) table.getModel();
			modelo.setNumRows(0);
			table.setModel(modelo);
			String linha = "";
			while ((linha = reader.readLine()) != null) {
				modelo.addRow(linha.split("-"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// EXPORTA CLIENTES QUE SAO PESSOAS FISICAS EM UM ARQUIVO DE BINARIO
	public void exportarClientesBinario() {

		File arquivo = new File("arquivoPessoasBinario.bin");
		try (OutputStream os = new FileOutputStream(arquivo)) {
			for (int i = 0; i < table.getRowCount(); i++) {
				for (int k = 0; k < table.getColumnCount(); k++) {
					os.write(table.getValueAt(i, k).toString().getBytes());
					os.write("-".getBytes());
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
	// IMPORTA CLIENTES QUE SAO PESSOAS FISICAS DE UM ARQUIVO DE BINARIO
	/*
	 * public void importarClientesBinario() {
	 * 
	 * try { Scanner scannerFuncionario = new Scanner(new
	 * FileReader("arquivoPessoasBinario.bin")).useDelimiter("-|\\n");
	 * 
	 * while (scannerFuncionario.hasNext()) { String nomeFuncionario, endereco,
	 * telefone, cpf;
	 * 
	 * nomeFuncionario = scannerFuncionario.next();
	 * funcionario.setNomeFuncionario(nomeFuncionario);
	 * 
	 * endereco = scannerFuncionario.next();
	 * funcionario.setCargo(cargo);(endereco);
	 * 
	 * telefone = scannerFuncionario.next(); funcionario.setTelefone(telefone);
	 * 
	 * cpf = scannerFuncionario.next(); funcionario.setCpf(cpf);
	 * 
	 * adicionarCliente(nomeFuncionario, endereco, telefone, cpf); } } catch
	 * (Exception ex)
	 * 
	 * { ex.getMessage(); }
	 * 
	 * }
	 */
}
