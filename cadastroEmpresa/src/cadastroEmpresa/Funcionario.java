package cadastroEmpresa;

import javax.swing.JTextField;

public class Funcionario {

	private JTextField nomeFuncionario = new JTextField();
	private JTextField nrRegistro = new JTextField();
	private JTextField vlSalario = new JTextField();
	private JTextField nrCpf = new JTextField();
	private JTextField nrRg = new JTextField();
	private JTextField cargo = new JTextField();

	
	public JTextField getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(JTextField nrCpf) {
		this.nrCpf = nrCpf;
	}

	public JTextField getNrRg() {
		return nrRg;
	}

	public void setNrRg(JTextField nrRg) {
		this.nrRg = nrRg;
	}

	public JTextField getCargo() {
		return cargo;
	}

	public void setCargo(JTextField cargo) {
		this.cargo = cargo;
	}

	public JTextField getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(JTextField nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public JTextField getNrRegistro() {
		return nrRegistro;
	}

	public void setNrRegistro(JTextField nrRegistro) {
		this.nrRegistro = nrRegistro;
	}

	public JTextField getVlSalario() {
		return vlSalario;
	}

	public void setVlSalario(JTextField vlSalario) {
		this.vlSalario = vlSalario;
	}

}
