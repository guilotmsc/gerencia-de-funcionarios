package cadastroEmpresa;

import java.sql.Connection;
import java.sql.DriverManager; 
import java.sql.SQLException;
import java.sql.Statement;

public class geradorSql {

	static Connection conexao;

	@SuppressWarnings("unused")
	private static Connection ObterConexao() throws SQLException {

		Connection conexao = null;

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			conexao = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "system", "vapraporra");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return conexao;
	}

	public static void executa(String sql) {
		Statement at = null;
		try {
			at = conexao.createStatement();
			at.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} 
	
}
